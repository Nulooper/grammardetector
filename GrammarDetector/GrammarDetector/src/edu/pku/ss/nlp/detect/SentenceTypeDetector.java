package edu.pku.ss.nlp.detect;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.nlp.grammar.BaseGrammar;
import edu.pku.ss.nlp.grammar.SentenceType;
import edu.pku.ss.nlp.units.LanguageUnit;

/**
 * 句子类型检测器. 句子类型根据用途可以分为四类：陈述句,疑问句,感叹句. 当前值考虑陈述句,疑问句和感叹句.
 * 
 * @author nulooper
 * @date 2015年4月11日
 * @time 下午10:36:35
 */
public class SentenceTypeDetector implements Detectable {

	private static final String SENTENCE_TYPE_PATH = "res/sentence_type/sentence_type.tsv";
	private Map<String, String> punctuationTypeMap;

	public SentenceTypeDetector() {
		this.punctuationTypeMap = new HashMap<String, String>();
		try {
			List<String> lines = FileUtils.readLines(new File(
					SENTENCE_TYPE_PATH));
			String[] items;
			String line;
			int size = lines.size();
			for (int i = 0; i < size; ++i) {
				line = lines.get(i);
				items = line.split("\t");
				if (items.length != 2) {
					Logger.getGlobal().logp(
							Level.WARNING,
							this.getClass().getName(),
							"SentenceTypeDetector",
							"文件[" + SENTENCE_TYPE_PATH + "]第" + (i + 1)
									+ "行格式不正确！");
				}
				this.punctuationTypeMap.put(items[0], items[1]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public Map<TokenPosition, BaseGrammar> detectGrammar(LanguageUnit unit) {
		Map<TokenPosition, BaseGrammar> grammarMap = new HashMap<TokenPosition, BaseGrammar>();
		List<String> tokenList = unit.getTokenList();

		if (tokenList.isEmpty()) {
			return Collections.unmodifiableMap(grammarMap);
		}

		int size = unit.size();
		String lastToken = tokenList.get(size - 1);
		if (this.punctuationTypeMap.containsKey(lastToken)) {
			grammarMap.put(new TokenPosition(-1), new SentenceType(
					this.punctuationTypeMap.get(lastToken)));
		}

		return Collections.unmodifiableMap(grammarMap);
	}

	public static void main(String[] args) {
		String sent = "I love you very much !";
		SentenceTypeDetector std = new SentenceTypeDetector();
		Map<TokenPosition, BaseGrammar> gm = std
				.detectGrammar(new LanguageUnit(sent));
		for (TokenPosition tp : gm.keySet()) {
			System.out.println(tp + "\t" + gm.get(tp));
		}
	}
}
