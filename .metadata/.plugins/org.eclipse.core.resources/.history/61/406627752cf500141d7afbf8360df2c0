package edu.pku.ss.nlp.detect;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.nlp.grammar.BaseGrammar;
import edu.pku.ss.nlp.grammar.POSGrammar;
import edu.pku.ss.nlp.units.LanguageUnit;
import edu.pku.ss.nlp.utils.Utils;

/**
 * 情态动词检测器. 目前只检测：名词, 冠词, 代词, 形容词, 副词, 介词, 副词.
 * 
 * @author nulooper
 * @date 2015年4月11日
 * @time 下午10:36:35
 */
public class POSDetector implements Detectable {
	private Map<String, String> tagNameMap;

	private static final String NAME_TAGS_PATH = "res/pos/name_tags.tsv";

	public POSDetector() {
		this.tagNameMap = new HashMap<String, String>();
		try {
			List<String> lines = FileUtils.readLines(new File(NAME_TAGS_PATH));
			String[] items, tags;

			int size = lines.size();
			String line;
			for (int i = 0; i < size; ++i) {
				line = lines.get(i);
				items = line.split("\t");
				if (items.length != 2) {
					Logger.getGlobal().logp(Level.WARNING,
							this.getClass().getName(), "POSDetector",
							NAME_TAGS_PATH + " 文件第" + (i + 1) + "行格式不正确!");
					continue;
				}

				tags = items[1].split("\\|");
				for (String tag : tags) {
					this.tagNameMap.put(tag, items[0]);
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Map<TokenPosition, Set<BaseGrammar>> detectGrammar(LanguageUnit unit) {
		Map<TokenPosition, Set<BaseGrammar>> grammarMap = new HashMap<TokenPosition, Set<BaseGrammar>>();
		List<String> posList = unit.getPOSList();

		int size = unit.size();
		String pos;
		for (int i = 0; i < size; ++i) {
			pos = posList.get(i);
			if (this.tagNameMap.containsKey(pos)) {
				Set<BaseGrammar> grammarSet = new HashSet<BaseGrammar>();
				grammarSet.add(new POSGrammar(tagNameMap.get(pos)));
				grammarMap.put(new TokenPosition(i + 1), grammarSet);
			}
		}

		return Collections.unmodifiableMap(grammarMap);
	}

	public static void main(String[] args) {
		LanguageUnit unit = new LanguageUnit("I love you !");
		POSDetector p = new POSDetector();
		Map<TokenPosition, Set<BaseGrammar>> gm = p.detectGrammar(unit);
		for(TokenPosition tp : gm.keySet()){
			System.out.println(tp.toString() + ":\t" + Utils.joinOn(new ArrayList<BaseGrammar>(grammarMap.get(tp)), "___"));
		}
	}
}
