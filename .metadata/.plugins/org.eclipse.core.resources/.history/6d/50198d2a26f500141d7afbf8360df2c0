package edu.pku.ss.nlp.detect;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import edu.pku.ss.nlp.grammar.BaseGrammar;
import edu.pku.ss.nlp.grammar.ModalVerb;
import edu.pku.ss.nlp.units.LanguageUnit;
import edu.pku.ss.nlp.utils.Utils;

/**
 * 情态动词检测.
 * 
 * @author nulooper
 * @date 2015年3月24日
 * @time 下午10:36:35
 */
public class ModalVerbDetector implements Detectable {

	private static final String MODEL_VERB_REGEX_PATH = "res/modal_verb/modal_verb_regex.txt";
	private List<Pattern> patterns;
	private Set<String> modalSet;

	public ModalVerbDetector() {
		try {
			List<String> lines = FileUtils.readLines(new File(
					MODEL_VERB_REGEX_PATH));
			this.patterns = new ArrayList<Pattern>();
			this.modalSet = new HashSet<String>();
			for (String line : lines) {
				String[] items = line.split("\\|");
				if (items.length != 2) {
					continue;
				}
				String modal = items[1].split("#")[0]; // token or regex
														// expression.
				if ("HASH".equals(items[0])) {
					this.modalSet.add(modal);
				} else {
					this.patterns.add(Pattern.compile(modal,
							Pattern.CASE_INSENSITIVE));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Map<TokenPosition, Set<BaseGrammar>> detectGrammar(LanguageUnit unit) {
		Map<TokenPosition, Set<BaseGrammar>> grammarMap = new HashMap<TokenPosition, Set<BaseGrammar>>();

		// 先检查情态词典，词典中的情态动词只包含一个词.
		List<String> lemmaList = unit.getLemmaList();
		int size = lemmaList.size();
		for (int i = 0; i < size; ++i) {
			if (this.modalSet.contains(lemmaList.get(i).toLowerCase())) {
				// 只检测第一个出现的情态动词，假设一句英文序列中只出现一个情态动词.
				Set<BaseGrammar> grammarSet = new HashSet<BaseGrammar>();
				grammarSet.add(new ModalVerb(lemmaList.get(i).toLowerCase()));
				grammarMap.put(new TokenPosition(i + 1), grammarSet);
				
				return grammarMap;
			}
		}

		// 使用正则表达式，检测情态动词短语.
		String joinedStr = Utils.joinOn(unit.getLemmaList(), " ").toLowerCase();
		Matcher matcher = null;
		for (Pattern pat : this.patterns) {
			matcher = pat.matcher(joinedStr);
			if (matcher.matches()) {
				// 只取取第一个组.
				Set<BaseGrammar> grammarSet = new HashSet<BaseGrammar>();
				grammarSet.add(new ModalVerb(pat.toString()));
				grammarMap.put(
						new TokenPosition(matcher.start(0), matcher.end(0)),
						);
				return grammarMap;
			}
		}

		return grammarMap;
	}

	public static void main(String[] args) {
		String sent = "Shall we watch the baseball game on TV?";
		ModalVerbDetector detector = new ModalVerbDetector();
		System.out.println(detector.toString());
		Map<TokenPosition, BaseGrammar> map = detector
				.detectGrammar(new LanguageUnit(sent));
		System.out.println(map.size());
		for (TokenPosition tp : map.keySet()) {
			System.out.println(tp.toString() + ":\t" + map.get(tp).toString());
		}
	}

}
